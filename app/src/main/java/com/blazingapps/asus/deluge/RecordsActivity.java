package com.blazingapps.asus.deluge;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RecordsActivity extends AppCompatActivity {

    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String JSON_DRAFT_LIST = "jsondraftlist";
    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    SharedPreferences sharedPreferences;
    ArrayList<FloodObject> myDataset;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        builder=new AlertDialog.Builder(this);
        builder.setMessage("Please wait").setCancelable(false);
        alertDialog=builder.create();

        sharedPreferences=getSharedPreferences(MYPREF,MODE_PRIVATE);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);



        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        myDataset=new ArrayList<>();

        String url = "https://us-central1-deluge-94c7f.cloudfunctions.net/getrecords?uid="+sharedPreferences.getString(USERKEY,null);

        mAdapter = new RecordAdapter(RecordsActivity.this,myDataset);
        mRecyclerView.setAdapter(mAdapter);

        if (!isnetworkconnected()){
            Toast.makeText(this,"No Internet",Toast.LENGTH_LONG).show();
            return;
        }

        new MyAsyncTask().execute(url);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    class MyAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
            if (!alertDialog.isShowing()){
                alertDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s==null){
                return;
            }
            //Log.d("TAG",s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getInt("status")==200){
                    JSONArray jsonArray = jsonObject.getJSONArray("flooddata");
                    for (int i=0 ; i < jsonArray.length(); ++i){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        FloodObject floodObject = new FloodObject(
                                jsonObject1.getString("latitude"),
                                jsonObject1.getString("longitude"),
                                jsonObject1.getString("city"),
                                jsonObject1.getString("height"),
                                jsonObject1.getString("altitude"),
                                jsonObject1.getString("referencekey")
                        );
                        myDataset.add(floodObject);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
                //Log.d("TAG",e.getMessage());
            }

            if (alertDialog.isShowing())
            {
                alertDialog.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(strings[0])
                        .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                return response.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public boolean isnetworkconnected()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return connected;
    }
}
