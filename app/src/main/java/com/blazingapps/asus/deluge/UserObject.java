package com.blazingapps.asus.deluge;

public class UserObject {
    String authkey;
    String name;
    String password;
    String phone;

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public UserObject(String name, String password, String phone, String authkey) {
        this.authkey=authkey;
        this.name = name;
        this.password = password;
        this.phone = phone;
    }
}
