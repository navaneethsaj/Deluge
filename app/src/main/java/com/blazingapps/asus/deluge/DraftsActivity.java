package com.blazingapps.asus.deluge;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class DraftsActivity extends AppCompatActivity {
    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String JSON_DRAFT_LIST = "jsondraftlist";
    SharedPreferences sharedPreferences;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView nothing;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drafts);

        sharedPreferences=getSharedPreferences(MYPREF,MODE_PRIVATE);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        nothing=findViewById(R.id.nothingtoshow);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        ArrayList<FloodObject> myDataset=new ArrayList<>();

        Gson gson = new Gson();
        String jsondata = sharedPreferences.getString(JSON_DRAFT_LIST,null);

        if (jsondata==null){
            Toast.makeText(getApplicationContext(),"No Saved Drafts",Toast.LENGTH_LONG).show();
            nothing.setText("Nothing to Show");
            return;
        }

        myDataset = gson.fromJson(jsondata,new TypeToken<List<FloodObject>>(){}.getType());

        mAdapter = new DraftAdapter(DraftsActivity.this,myDataset);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
