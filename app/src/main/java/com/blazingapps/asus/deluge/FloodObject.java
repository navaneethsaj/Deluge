package com.blazingapps.asus.deluge;

public class FloodObject {
    String latitude;
    String longitude;
    String city;
    String height;
    String altitude;
    String referencekey;

    public FloodObject(String latitude, String longitude, String city, String height, String altitude, String referencekey) {
        this.latitude = latitude;
        this.referencekey=referencekey;
        this.longitude = longitude;
        this.city = city;
        this.altitude = altitude;
        this.height = height;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getCity() {
        return city;
    }

    public String getHeight() {
        return height;
    }

    public String getAltitude() {
        return altitude;
    }

    public String getReferencekey(){
        return referencekey;
    }
}
