package com.blazingapps.asus.deluge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;

public class DraftAdapter extends RecyclerView.Adapter<DraftAdapter.MyViewHolder> {

    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String JSON_DRAFT_LIST = "jsondraftlist";
    private ArrayList<FloodObject> floodObjectArrayList;
    FirebaseDatabase database;
    DatabaseReference flooddataRef,userRef;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Activity activity;
    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView latitude, longitude, city, height, altitude;
        Button sendbutton,removebutton;

        public MyViewHolder(View view) {
            super(view);
            removebutton=(Button)view.findViewById(R.id.remove);
            sendbutton=(Button)view.findViewById(R.id.send);
            latitude = (TextView) view.findViewById(R.id.latitude);
            longitude = (TextView) view.findViewById(R.id.longitude);
            city = (TextView) view.findViewById(R.id.city);
            height = (TextView) view.findViewById(R.id.height);
            altitude = (TextView)view.findViewById(R.id.altitude);
        }
    }


    public DraftAdapter(Activity activity,ArrayList<FloodObject> floodObjectArrayList) {
        this.floodObjectArrayList = floodObjectArrayList;
        this.activity = activity;

        sharedPreferences=activity.getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        database=FirebaseDatabase.getInstance();

        userRef=database.getReference("users").child(sharedPreferences.getString(USERKEY,null));
        flooddataRef=database.getReference("flooddata");

        builder=new AlertDialog.Builder(activity);
        builder.setCancelable(false).setMessage("Sending");
        alertDialog=builder.create();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.draftitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FloodObject object = floodObjectArrayList.get(position);
        if (object==null){
            return;
        }
        holder.latitude.setText(""+object.getLatitude());
        holder.longitude.setText(""+object.getLongitude());
        holder.city.setText(""+object.getCity());
        holder.height.setText(""+object.getHeight());
        holder.altitude.setText(""+object.getAltitude());

        holder.sendbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!alertDialog.isShowing()){
                    alertDialog.show();
                }
                if (!isnetworkconnected()){
                    Toast.makeText(activity,"No Internet",Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                    return;
                }
                holder.sendbutton.setEnabled(false);
                holder.sendbutton.setText("Sending");
                DatabaseReference itemref = flooddataRef.push();
                if (itemref==null){
                    Toast.makeText(activity,"Send Failed",Toast.LENGTH_SHORT).show();
                    holder.sendbutton.setEnabled(true);
                    holder.sendbutton.setText("Send");
                    return;
                }
                String latitude=""+holder.latitude.getText().toString();
                String longitude=""+holder.longitude.getText().toString();
                String city=""+holder.city.getText().toString();
                String height=""+holder.height.getText().toString();
                String altitude =""+ holder.altitude.getText().toString();
                String itemkey=""+itemref.getKey();
                itemref.setValue(new FloodObject(latitude,longitude,city,height,altitude,itemkey)).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        userRef.child("datakeys").push().setValue(itemkey).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(activity,"Send Successful",Toast.LENGTH_SHORT).show();
                                floodObjectArrayList.remove(position);
                                Gson gson= new Gson();
                                String rawjson=gson.toJson(floodObjectArrayList);
                                //Log.d("rawjson",rawjson);
                                editor.putString(JSON_DRAFT_LIST,rawjson);
                                editor.commit();

                                notifyItemRemoved(position);
                                notifyItemChanged(position);
                                notifyDataSetChanged();

                                holder.sendbutton.setEnabled(true);
                                holder.sendbutton.setText("Send");

                                if (alertDialog.isShowing()){
                                    alertDialog.dismiss();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(activity,"Send Failed",Toast.LENGTH_SHORT).show();

                                holder.sendbutton.setEnabled(true);
                                holder.sendbutton.setText("Send");

                                if (alertDialog.isShowing()){
                                    alertDialog.dismiss();
                                }
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(activity,"Send Failed",Toast.LENGTH_SHORT).show();

                        if (alertDialog.isShowing()){
                            alertDialog.dismiss();
                        }
                        holder.sendbutton.setEnabled(true);
                        holder.sendbutton.setText("Send");
                    }
                });
            }
        });
        holder.removebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floodObjectArrayList.remove(position);
                Gson gson= new Gson();
                String rawjson=gson.toJson(floodObjectArrayList);
                //Log.d("rawjson",rawjson);
                editor.putString(JSON_DRAFT_LIST,rawjson);
                editor.commit();
                notifyItemRemoved(position);
                notifyDataSetChanged();
                notifyItemChanged(position);
            }
        });
    }

    public boolean isnetworkconnected()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return connected;
    }

    @Override
    public int getItemCount() {
        return floodObjectArrayList.size();
    }
}