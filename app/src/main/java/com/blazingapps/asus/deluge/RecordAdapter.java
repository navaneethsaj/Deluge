package com.blazingapps.asus.deluge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.MyViewHolder> {

    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String JSON_DRAFT_LIST = "jsondraftlist";
    private ArrayList<FloodObject> floodObjectArrayList;
    FirebaseDatabase database;
    DatabaseReference flooddataRef,userRef;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Activity activity;
    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView latitude, longitude, city, altitude;
        EditText height;
        Button updatebutton;

        public MyViewHolder(View view) {
            super(view);
            updatebutton=(Button)view.findViewById(R.id.update);
            latitude = (TextView) view.findViewById(R.id.latitude);
            longitude = (TextView) view.findViewById(R.id.longitude);
            city = (TextView) view.findViewById(R.id.city);
            height = (EditText) view.findViewById(R.id.height);
            altitude=(TextView)view.findViewById(R.id.altitude);
        }
    }


    public RecordAdapter(Activity activity,ArrayList<FloodObject> floodObjectArrayList) {
        this.floodObjectArrayList = floodObjectArrayList;
        this.activity = activity;

        sharedPreferences=activity.getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        database=FirebaseDatabase.getInstance();

        userRef=database.getReference("users").child(sharedPreferences.getString(USERKEY,null));
        flooddataRef=database.getReference("flooddata");

        builder=new AlertDialog.Builder(activity);
        builder.setCancelable(false).setMessage("Sending");
        alertDialog=builder.create();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recorditem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FloodObject object = floodObjectArrayList.get(position);
        holder.latitude.setText(object.getLatitude());
        holder.longitude.setText(object.getLongitude());
        holder.city.setText(object.getCity());
        holder.height.setText(object.getHeight());
        holder.altitude.setText(""+object.getAltitude());
        holder.updatebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!alertDialog.isShowing()){
                    alertDialog.show();
                }

                if (!isnetworkconnected()){
                    Toast.makeText(activity,"No Internet",Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                    return;

                }
                holder.updatebutton.setEnabled(false);
                holder.updatebutton.setText("Sending");

                DatabaseReference itemref = flooddataRef.child(floodObjectArrayList.get(position).getReferencekey());
                itemref.child("height").setValue(holder.height.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (alertDialog.isShowing()){
                            alertDialog.dismiss();
                            holder.updatebutton.setEnabled(true);
                            holder.updatebutton.setText("Update");
                            Toast.makeText(activity,"Updated",Toast.LENGTH_LONG).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (alertDialog.isShowing()){
                            alertDialog.dismiss();
                            holder.updatebutton.setEnabled(true);
                            holder.updatebutton.setText("Update");
                            Toast.makeText(activity,"Updated",Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

    }

    public boolean isnetworkconnected()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return connected;
    }
    @Override
    public int getItemCount() {
        return floodObjectArrayList.size();
    }
}
