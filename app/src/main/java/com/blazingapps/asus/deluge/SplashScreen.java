package com.blazingapps.asus.deluge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final long SPLASH_TIME_OUT = 1000;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sharedPreferences=getSharedPreferences(MYPREF,MODE_PRIVATE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPreferences.getBoolean(LOGGEDIN,false)){
                    Intent i = new Intent(SplashScreen.this, Survey.class);
                    startActivity(i);
                    finish();
                }else {

                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
