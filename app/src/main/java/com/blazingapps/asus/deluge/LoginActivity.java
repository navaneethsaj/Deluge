package com.blazingapps.asus.deluge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {
    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";
    private static final String AUTHKEY="authkey";
    Button signup, signin;
    EditText userEdittext,passwordEdittext,contactEdittext;
    FirebaseDatabase database;
    DatabaseReference myRef;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference("users");
        mAuth = FirebaseAuth.getInstance();
        sharedPreferences=getSharedPreferences(MYPREF,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        signin=findViewById(R.id.signin);
        signup=findViewById(R.id.signup);
        userEdittext=findViewById(R.id.user);
        passwordEdittext=findViewById(R.id.password);
        contactEdittext=findViewById(R.id.phone);
        final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
        signin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        view.getBackground().setColorFilter(getResources().getColor(R.color.colorbuttonpress), PorterDuff.Mode.SRC_ATOP);                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
        signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    view.getBackground().setColorFilter(getResources().getColor(R.color.colorbuttonpress), PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    view.getBackground().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }
                return false;
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username=userEdittext.getText().toString();
                String password=passwordEdittext.getText().toString();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isnetworkconnected()){
                    Toast.makeText(getApplicationContext(),"No Internet",Toast.LENGTH_LONG).show();
                    return;
                }
                String username=userEdittext.getText().toString();
                String password=passwordEdittext.getText().toString();
                String contact=contactEdittext.getText().toString();
                if (sharedPreferences.getString(USERKEY,null)==null){
                    DatabaseReference userref = myRef.push();
                    String key= userref.getKey();
                    if (key!=null){
                        editor.putString(USERKEY,key);
                        editor.putString(USERNAME,username);
                        editor.putString(ACC_PASSWORD,password);
                        editor.putString(PHONE,contact);
                        editor.commit();
                        userref.setValue(new UserObject(username,password,contact,sharedPreferences.getString(AUTHKEY,null))).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getApplicationContext(),"Registered",Toast.LENGTH_SHORT).show();
                                surveyIntent();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Server returned null key",Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    DatabaseReference userref=myRef.child(sharedPreferences.getString(USERKEY,null));
                    userref.setValue(new UserObject(username,password,contact,sharedPreferences.getString(AUTHKEY,null))).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(),"Registered",Toast.LENGTH_SHORT).show();
                            surveyIntent();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        createuser(currentUser);
    }

    public void surveyIntent(){
        editor.putBoolean(LOGGEDIN,true);
        editor.commit();
        Intent intent = new Intent(this,Survey.class);
        startActivity(intent);
        finish();
    }

    private void createuser(FirebaseUser currentUser) {
        if (currentUser==null){
            mAuth.signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    //Log.d("tag",authResult.getUser().getUid());
                    editor.putString(AUTHKEY,authResult.getUser().getUid());
                    editor.commit();
                    Toast.makeText(getApplicationContext(),"Authentication Successful",Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //Log.d("tag",e.toString());
                    Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),"Authenticated Device",Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isnetworkconnected()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return connected;
    }

}
