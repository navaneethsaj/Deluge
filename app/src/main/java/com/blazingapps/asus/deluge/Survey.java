package com.blazingapps.asus.deluge;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Survey extends AppCompatActivity implements LocationListener {
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String JSON_DRAFT_LIST = "jsondraftlist";
    private static final int REQUEST_CODE_AUTOCOMPLETE = 0;
    private MapView mapView;
    Button records, viewdrafts, savedraft, send, usegps, getlastlocationButton, nwgps
            ;
    TextView latitudetextview, longitudetextview, citytextview, altitudetextview;
    EditText heightedittext;
    private static final String MYPREF = "userpref";
    private static final String USERKEY = "userkey";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";
    private static final String ACC_PASSWORD = "password";
    private static final String LOGGEDIN = "loggedin";

    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference flooddataRef, userRef;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    AlertDialog alertDialog, senddialog, altitudedialog,nwdialog,gpedialog;
    AlertDialog.Builder builder, sendbuilder, altitudebuilder;
    FloatingActionButton streetfab, satellitefab;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1IjoibmF2YW5lZXRoc2FqIiwiYSI6ImNqbjM1cDhobjJkMTAzcXF2eTh2cXdnOHcifQ.owucQ0sKQs6gnDb_2MmbYg");
        setContentView(R.layout.activity_survey);
        sharedPreferences = getSharedPreferences(MYPREF, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        builder = new AlertDialog.Builder(this);
        sendbuilder = new AlertDialog.Builder(this);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users").child(sharedPreferences.getString(USERKEY, null));
        flooddataRef = database.getReference("flooddata");
        records = findViewById(R.id.viewrecords);
        viewdrafts = findViewById(R.id.viewdrafts);
        savedraft = findViewById(R.id.savedraft);
        send = findViewById(R.id.send);
        altitudetextview = findViewById(R.id.altitude);
        usegps = findViewById(R.id.usegps);
        nwgps = findViewById(R.id.nwgps);
        citytextview = findViewById(R.id.citytextview);
        latitudetextview = findViewById(R.id.latitude);
        longitudetextview = findViewById(R.id.longitude);
        heightedittext = findViewById(R.id.heightedittext);
        getlastlocationButton = findViewById(R.id.lastlocation);
        satellitefab = findViewById(R.id.satellitefab);
        streetfab = findViewById(R.id.streetmapfab);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        AlertDialog.Builder gebuilder = new AlertDialog.Builder(Survey.this);
        gebuilder.setMessage("Turn On GPS and click ok").setCancelable(false).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });

        gpedialog=gebuilder.create();
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.addOnMapClickListener(new MapboxMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(@NonNull LatLng point) {
                        mapboxMap.clear();
                        Location location = new Location(LocationManager.GPS_PROVIDER);
                        location.setLatitude(point.getLatitude());
                        location.setLongitude(point.getLongitude());
                        //Toast.makeText(getApplicationContext(), String.valueOf(location.getAltitude()), Toast.LENGTH_SHORT).show();
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(point)
                                .title("Here")
                                .snippet(getcityname(location))
                        );
                        latitudetextview.setText("" + point.getLatitude());
                        longitudetextview.setText("" + point.getLongitude());


                        new AltitudeAsyncTask().execute("(" + location.getLatitude() + "," + location.getLongitude() + ")");
                    }
                });
            }
        });

        usegps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(Survey.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST);
                    return;
                }

                if (!isGPSEnabled(getApplicationContext())){

                    if (!gpedialog.isShowing()){
                        gpedialog.show();
                        return;
                    }
                }

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, Survey.this);
                if (!alertDialog.isShowing()){
                    alertDialog.show();
                }
            }
        });

        nwgps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(Survey.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST);
                    return;
                }
                if (!isGPSEnabled(getApplicationContext())){

                    if (!gpedialog.isShowing()){
                        gpedialog.show();
                        return;
                    }
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, Survey.this);
                if (!nwdialog.isShowing()){
                    nwdialog.show();
                }
            }
        });

        getlastlocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Survey.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(Survey.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST);
                    return;
                }

                if (!isGPSEnabled(getApplicationContext())){

                    if (!gpedialog.isShowing()){
                        gpedialog.show();
                        return;
                    }
                }

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (lastKnownLocation==null){
                    Toast.makeText(getApplicationContext(),"Last Location Not Available", Toast.LENGTH_SHORT).show();
                    return;
                }

                latitudetextview.setText("" + lastKnownLocation.getLatitude());
                longitudetextview.setText("" + lastKnownLocation.getLongitude());
                citytextview.setText("" + getcityname(lastKnownLocation));
                new AltitudeAsyncTask().execute("("+lastKnownLocation.getLatitude()+","+lastKnownLocation.getLongitude()+")");

                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(MapboxMap mapboxMap) {
                        mapboxMap.clear();

                        CameraPosition newCameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()))
                                .zoom(14)
                                .build();

                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition), 4000);
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()))
                                .title("Here")
                        );
                    }
                });
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isnetworkconnected()) {
                    Toast.makeText(getApplicationContext(), "No Internet", Toast.LENGTH_LONG).show();
                    return;
                }
                if (heightedittext.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Enter Height", Toast.LENGTH_LONG).show();
                    return;
                }
                if (latitudetextview.getText().toString().equals("Latitude") && longitudetextview.getText().toString().equals("Longitude")){
                    Toast.makeText(getApplicationContext(), "Select Location", Toast.LENGTH_LONG).show();
                    return;
                }
                senddata();
            }
        });

        savedraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (heightedittext.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Enter Height", Toast.LENGTH_LONG).show();
                    return;
                }

                if (latitudetextview.getText().toString().equals("Latitude") && longitudetextview.getText().toString().equals("Longitude")){
                    Toast.makeText(getApplicationContext(), "Select Location", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    savearraylist();
                    Toast.makeText(getApplicationContext(), "Saved in Draft", Toast.LENGTH_SHORT).show();
                    heightedittext.setText("");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        viewdrafts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Survey.this, DraftsActivity.class);
                startActivity(intent);
            }
        });

        records.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Survey.this, RecordsActivity.class);
                startActivity(intent);
            }
        });

        streetfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(MapboxMap mapboxMap) {

                        mapboxMap.setStyleUrl("mapbox://styles/navaneethsaj/cjn42gmxe1ho32sp8k1uj8v5s");
                    }
                });
            }
        });

        satellitefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(MapboxMap mapboxMap) {

                        mapboxMap.setStyleUrl("mapbox://styles/navaneethsaj/cjn42n0n4003k2rpdhmmr9v6z");
                    }
                });
            }
        });

        builder.setMessage("Using Satellite Provider...").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                locationManager.removeUpdates(Survey.this);
            }
        }).setCancelable(false);

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Using Network Location...").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                locationManager.removeUpdates(Survey.this);
            }
        }).setCancelable(false);


        altitudebuilder = new AlertDialog.Builder(this);
        altitudebuilder.setMessage("Receiving Altitude Details").setCancelable(false);
        altitudedialog = altitudebuilder.create();

        sendbuilder.setMessage("Sending").setCancelable(false);
        senddialog = sendbuilder.create();

        alertDialog = builder.create();
        nwdialog=builder1.create();


        initSearchFab();
    }

    private void savearraylist() throws JSONException {
        String latitude = ""+latitudetextview.getText().toString();
        String longitude = ""+longitudetextview.getText().toString();
        String city = ""+citytextview.getText().toString();
        String height = ""+heightedittext.getText().toString();
        String altitude = ""+altitudetextview.getText().toString();
        FloodObject floodObject = new FloodObject(latitude, longitude, city, height, altitude, null);

        ArrayList<FloodObject> floodObjectArrayList = new ArrayList<>();

        String jsondata = sharedPreferences.getString(JSON_DRAFT_LIST, null);

        Gson gson = new Gson();
        if (jsondata == null) {
            //Log.d("control", String.valueOf(1));
            floodObjectArrayList.add(floodObject);
            String rawjson = gson.toJson(floodObjectArrayList);
            //Log.d("rawjson", rawjson);
            editor.putString(JSON_DRAFT_LIST, rawjson);
            editor.commit();
        } else {
            //Log.d("control", String.valueOf(2));
            floodObjectArrayList = gson.fromJson(jsondata, new TypeToken<List<FloodObject>>() {
            }.getType());
            floodObjectArrayList.add(floodObject);
            String rawjson = gson.toJson(floodObjectArrayList);
            //Log.d("rawjson", rawjson);
            editor.putString(JSON_DRAFT_LIST, rawjson);
            editor.commit();
        }
        //Log.d("gson", gson.toJson(floodObjectArrayList));

        //Log.d("final", sharedPreferences.getString(JSON_DRAFT_LIST, "nodata"));
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        createuser(currentUser);

    }

    private void createuser(FirebaseUser currentUser) {
        if (currentUser==null){
            mAuth.signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    //Log.d("tag",authResult.toString());
                    userRef.child("authtoken").setValue(authResult.getUser().getUid());
                    Toast.makeText(getApplicationContext(),"Authentication Successful",Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_SHORT).show();
                }
            });
        }else {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudetextview.setText(""+location.getLatitude());
        longitudetextview.setText(""+location.getLongitude());
        citytextview.setText(""+getcityname(location));
        new AltitudeAsyncTask().execute("("+location.getLatitude()+","+location.getLongitude()+")");

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.clear();
                CameraPosition newCameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(12)
                        .build();

                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition), 4000);
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(location.getLatitude(),location.getLongitude()))
                        .title("Here")
                );
            }
        });
        if (alertDialog.isShowing()){
            alertDialog.dismiss();
        }
        if (nwdialog.isShowing()){
            nwdialog.dismiss();
        }
        locationManager.removeUpdates(Survey.this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

        //Log.d("TAG",s);
    }

    @Override
    public void onProviderEnabled(String s) {
        //Log.d("TAG",s);
    }

    @Override
    public void onProviderDisabled(String s) {

        //Log.d("TAG",s);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted

                } else {
                    Toast.makeText(getApplicationContext(),"Permission Denied",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.clear();
                if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);

                    FeatureCollection featureCollection = FeatureCollection.fromFeatures(
                            new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())});

                    CameraPosition newCameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                    ((Point) selectedCarmenFeature.geometry()).longitude()))
                            .zoom(11)
                            .build();
                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition), 4000);
                    mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                    ((Point) selectedCarmenFeature.geometry()).longitude()))
                            .title("Here")
                    );
                    LatLng latLng = new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                            ((Point) selectedCarmenFeature.geometry()).longitude());
                    latitudetextview.setText(""+ latLng.getLatitude());
                    longitudetextview.setText(""+latLng.getLongitude());
                    new AltitudeAsyncTask().execute("("+latLng.getLatitude()+","+latLng.getLongitude()+")");

                    Location targetLocation = new Location("");
                    targetLocation.setLatitude(latLng.getLatitude());
                    targetLocation.setLongitude(latLng.getLongitude());


                    citytextview.setText(""+getcityname(targetLocation));
                }
            }
        });
    }

    public String getcityname(Location loc){
        String cityName=null;
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(loc.getLatitude(),
                    loc.getLongitude(), 1);
            if (addresses.size() > 0) {
                System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        citytextview.setText(cityName);
        return cityName;
    }

    public void senddata()
    {
        if (!senddialog.isShowing()){
            senddialog.show();
        }
        DatabaseReference itemref = flooddataRef.push();
        String latitude=""+latitudetextview.getText().toString();
        String longitude=""+longitudetextview.getText().toString();
        String city=""+citytextview.getText().toString();
        String height=""+heightedittext.getText().toString();
        String itemkey=""+itemref.getKey();
        String altitude=""+altitudetextview.getText().toString();
        itemref.setValue(new FloodObject(latitude,longitude,city,height,altitude,itemkey)).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                userRef.child("datakeys").push().setValue(itemkey).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(Survey.this,"Send Successful",Toast.LENGTH_SHORT).show();
                        heightedittext.setText("");
                        if (senddialog.isShowing()){
                            senddialog.dismiss();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Survey.this,"Send Failed",Toast.LENGTH_SHORT).show();
                        if (senddialog.isShowing()){
                            senddialog.dismiss();
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Survey.this,"Send Failed",Toast.LENGTH_SHORT).show();
                if (senddialog.isShowing()){
                    senddialog.dismiss();
                }
            }
        });
    }

    public boolean isnetworkconnected()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            connected = true;
        }
        else
            connected = false;

        return connected;
    }

    class AltitudeAsyncTask extends AsyncTask<String, Void, String >{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("https://elevation-api.io/api/elevation?points="+strings[0])
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                return response.body().string();
            } catch (Exception e) {
                e.printStackTrace();
                //Log.d("TAG",e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!altitudedialog.isShowing())
            {
                altitudedialog.show();
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (altitudedialog.isShowing()){
                altitudedialog.dismiss();
            }
            if (s==null){
                return;
            }
            JSONObject jsonObject= null;
            try {
                jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("elevations");
                JSONObject jsonObject1=jsonArray.getJSONObject(0);
                altitudetextview.setText(jsonObject1.getString("elevation"));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private void initSearchFab() {
        FloatingActionButton searchFab = findViewById(R.id.searchfab);
        searchFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken())
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                .limit(10)
                                .build(PlaceOptions.MODE_CARDS))
                        .build(Survey.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
            }
        });
    }

    public boolean isGPSEnabled (Context mContext){
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
